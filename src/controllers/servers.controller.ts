import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Servers} from '../models';
import {ServersRepository} from '../repositories';

export class ServersController {
  constructor(
    @repository(ServersRepository)
    public serversRepository : ServersRepository,
  ) {}

  @post('/servers/add', {
    responses: {
      '200': {
        description: 'Servers model instance',
        content: {'application/json': {schema: {'x-ts-type': Servers}}},
      },
    },
  })
  async create(@requestBody() servers: Servers): Promise<Servers> {
    return await this.serversRepository.create(servers);
  }

  @get('/servers', {
    responses: {
      '200': {
        description: 'Array of Servers model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Servers}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Servers)) filter?: Filter<Servers>,
  ): Promise<Servers[]> {
    return await this.serversRepository.find(filter);
  }

  @patch('/servers', {
    responses: {
      '200': {
        description: 'Servers PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() servers: Servers,
    @param.query.object('where', getWhereSchemaFor(Servers)) where?: Where<Servers>,
  ): Promise<Count> {
    return await this.serversRepository.updateAll(servers, where);
  }

  @get('/servers/{id}', {
    responses: {
      '200': {
        description: 'Servers model instance',
        content: {'application/json': {schema: {'x-ts-type': Servers}}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Servers> {
    return await this.serversRepository.findById(id);
  }

  @patch('/servers/{id}', {
    responses: {
      '204': {
        description: 'Servers PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody() servers: Servers,
  ): Promise<void> {
    await this.serversRepository.updateById(id, servers);
  }

  @put('/servers/{id}', {
    responses: {
      '204': {
        description: 'Servers PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() servers: Servers,
  ): Promise<void> {
    await this.serversRepository.replaceById(id, servers);
  }

  @del('/servers/{id}', {
    responses: {
      '204': {
        description: 'Servers DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.serversRepository.deleteById(id);
  }
}
