import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Users} from '../models';
import {UsersRepository} from '../repositories';

export class UsersController {
  constructor(
    @repository(UsersRepository)
    public usersRepository : UsersRepository,
  ) {}

  @post('/users/add', {
    responses: {
      '204': {
        description: 'Users model instance',
        content: {'application/json': {schema: {'x-ts-type': Users}}},
      },
    },
  })
  async create(@requestBody() users: Users): Promise<Users> {
    return await this.usersRepository.create(users);
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of Users model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Users}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Users)) filter?: Filter<Users>,
  ): Promise<Users[]> {
    return await this.usersRepository.find(filter);
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'Users PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() users: Users,
    @param.query.object('where', getWhereSchemaFor(Users)) where?: Where<Users>,
  ): Promise<Count> {
    return await this.usersRepository.updateAll(users, where);
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'Users model instance',
        content: {'application/json': {schema: {'x-ts-type': Users}}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Users> {
    return await this.usersRepository.findById(id);
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody() users: Users,
  ): Promise<void> {
    await this.usersRepository.updateById(id, users);
  }

  @put('/users/{id}', {
    responses: {
      '204': {
        description: 'Users PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() users: Users,
  ): Promise<void> {
    await this.usersRepository.replaceById(id, users);
  }

  @del('/users/{id}', {
    responses: {
      '204': {
        description: 'Users DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.usersRepository.deleteById(id);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: "Users Login",
        content: {'application/json': {schema: {'x-ts-type': Users}}},
      },
      '400': {
        description: "User no created or wrong password",
        content: {'application/json': {"Error": "400", "Message": "User no created or wrong password"}},
      }
    }
  })
  async loginUser(@requestBody() email: String, password: String): Promise<boolean> { 
    let user = await this.usersRepository.findOne({where: {email:`${email}`, password: `${password}`}})
    if (user) {
      return true
    } else {
      return false
    }
  }

  @get('/users/{username}', {
    responses: {
      '200': {
        description: "Find user",
        content: {'application/json': {schema: {'x-ts-type': Users}}},
      }
    }
  })
  async findUserByUsername(@param.path.string('param') userName: string) { 
    return await this.usersRepository.findOne({where: {username: userName}});
  }
}
