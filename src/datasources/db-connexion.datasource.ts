import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './db-connexion.datasource.json';

export class DbConnexionDataSource extends juggler.DataSource {
  static dataSourceName = 'db_connexion';

  constructor(
    @inject('datasources.config.db_connexion', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
