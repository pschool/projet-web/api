import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Servers extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  _id?: string;

  @property({
    type: 'string',
    required: true,
  })
  hostname: string;

  @property({
    type: 'string',
    required: true,
  })
  ipAddress: string;

  @property({
    type: 'string',
    required: true,
  })
  monitUser: string;

  @property({
    type: 'string',
    required: true,
  })
  monitSecret: string;

  @property({
    type: 'string',
    required: true,
  })
  osType: string;

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  health: string[];

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  services: string[];


  constructor(data?: Partial<Servers>) {
    super(data);
  }
}

export interface ServersRelations {
  // describe navigational properties here
}

export type ServersWithRelations = Servers & ServersRelations;
