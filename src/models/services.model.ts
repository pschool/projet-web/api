import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Services extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  _id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  check: string;

  @property({
    type: 'boolean',
    default: false,
    required: true,
  })
  default: boolean;


  constructor(data?: Partial<Services>) {
    super(data);
  }
}

export interface ServicesRelations {
  // describe navigational properties here
}

export type ServicesWithRelations = Services & ServicesRelations;
