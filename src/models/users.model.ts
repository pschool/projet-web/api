import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Users extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  _id?: string;

  @property({
    type: 'string',
  })
  firstname?: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
    required: true,
    default: 'this is a good salt',
  })
  salt: string;

  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  admin: boolean;

  @property({
    type: 'array',
    itemType: 'string',
  })
  server?: string[];


  constructor(data?: Partial<Users>) {
    super(data);
  }
}

export interface UsersRelations {
  // describe navigational properties here
}

export type UsersWithRelations = Users & UsersRelations;
