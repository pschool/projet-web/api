import {DefaultCrudRepository} from '@loopback/repository';
import {Servers, ServersRelations} from '../models';
import {DbConnexionDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ServersRepository extends DefaultCrudRepository<
  Servers,
  typeof Servers.prototype._id,
  ServersRelations
> {
  constructor(
    @inject('datasources.db_connexion') dataSource: DbConnexionDataSource,
  ) {
    super(Servers, dataSource);
  }
}
