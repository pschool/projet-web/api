import {DefaultCrudRepository} from '@loopback/repository';
import {Services, ServicesRelations} from '../models';
import {DbConnexionDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ServicesRepository extends DefaultCrudRepository<
  Services,
  typeof Services.prototype._id,
  ServicesRelations
> {
  constructor(
    @inject('datasources.db_connexion') dataSource: DbConnexionDataSource,
  ) {
    super(Services, dataSource);
  }
}
