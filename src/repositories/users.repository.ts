import {DefaultCrudRepository} from '@loopback/repository';
import {Users, UsersRelations} from '../models';
import {DbConnexionDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UsersRepository extends DefaultCrudRepository<
  Users,
  typeof Users.prototype._id,
  UsersRelations
> {
  constructor(
    @inject('datasources.db_connexion') dataSource: DbConnexionDataSource,
  ) {
    super(Users, dataSource);
  }
}
